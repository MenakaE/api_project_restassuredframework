package Driver_Package;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import Common_Utility_Methods.Excel_Data_Extractor;

public class Dynamic_Driver_Class {
	public static void main(String[] args)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		ArrayList<String> TC_Execute = Excel_Data_Extractor.Read_Excel_Data("Test_Data_API", "Test_Cases", "TC_Name");
		System.out.println(TC_Execute);
		int count = TC_Execute.size();		
		for (int i = 1; i < count; i++) {
			String TC_Name = TC_Execute.get(i);
			System.out.println(TC_Name);

			// Call the Test case class on runtime by using java.lang.reflect package
			Class<?> TestClass = Class.forName("API_TestSuite." + TC_Name);

			// Call the execute method belonging to test class captured in variable
			// (TC_Name)Test_class_name by using java.lang.reflect.method class
			Method Execute_Method = TestClass.getDeclaredMethod("executor");

			// Set the accessibility of method true
			Execute_Method.setAccessible(true);;
 
			// Create the instance of test class captured in variable name test classname
			Object Instance_of_TestClass = TestClass.getDeclaredConstructor().newInstance();

			// Execute the test script class fetched in variable TestClass
			Execute_Method.invoke(Instance_of_TestClass);
		}
	}
}
