package Driver_Package;

import java.io.IOException;

import API_TestSuite.Delete_TestScript;
import API_TestSuite.Get_TestScript;
import API_TestSuite.Patch_TestScript;
import API_TestSuite.Post_TestScript;
import API_TestSuite.Put_TestScript;

public class Driver_Class {

	public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException {

		Post_TestScript.executor();
		Put_TestScript.executor();
		Get_TestScript.executor();
		Patch_TestScript.executor();
		Delete_TestScript.executor();
		
		
	}
}
