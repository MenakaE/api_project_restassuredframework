package API_TestSuite;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.Test;

import API_Common_Methods.Common_method_handle_API;
import Common_Utility_Methods.Create_directory;
import Common_Utility_Methods.Handle_API_logs;
import Endpoint_Repository.Delete_endpoint;

public class Delete_TestScript extends Common_method_handle_API {
	@Test
	public static void executor() throws IOException {

		File log_dir = Create_directory.Create_log_directory("Delete_TestScript_logs");

		String delete_endpoint = Delete_endpoint.delete_endpoint_test1();

		for (int i = 0; i < 5; i++) {
			int delete_statusCode = delete_statusCode(delete_endpoint);
			System.out.println(delete_statusCode);

			if (delete_statusCode == 204) {
				Handle_API_logs.evidence_creation(log_dir, "Delete_TestScript", delete_endpoint);
				break;
			} else {
				System.out.println("Expected status code for delete method (204) is not found, hence retrying");
			}
		}
	}
}
