package Common_Utility_Methods;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;

public class Handle_API_logs {

	public static void evidence_creation(File dir_name, String File_name, String endpoint, String requestBody,
			String responseBody) throws IOException {
		String current_time = LocalDateTime.now().toString();
		File newFile = new File(dir_name + "\\" + File_name + ".txt");
		System.out
				.println("To save request and response body , we have created a new file named : " + newFile.getName());

		FileWriter dataWriter = new FileWriter(newFile);

		dataWriter.write("Endpoint is : " + endpoint + "\n");
		dataWriter.write("Request Body is : " + requestBody + "\n");
		dataWriter.write("Response Body is : " + responseBody);
		dataWriter.close();
	}

	public static void evidence_creation(File dir_name, String File_name, String endpoint, String responseBody)
			throws IOException {
		String current_time = LocalDateTime.now().toString();
		File newFile = new File(dir_name + "\\" + File_name + ".txt");
		System.out.println("To save response body , we have created a new file named : " + newFile.getName());

		FileWriter dataWriter = new FileWriter(newFile);

		dataWriter.write("Endpoint is : " + endpoint + "\n");
		dataWriter.write("Response Body is : " + responseBody);
		dataWriter.close();
	}

	public static void evidence_creation(File dir_name, String File_name, String endpoint) throws IOException {
		String current_time = LocalDateTime.now().toString();
		File newFile = new File(dir_name + "\\" + File_name + ".txt");
		System.out.println("To save API triggered, we have created a new file named : " + newFile.getName());

		FileWriter dataWriter = new FileWriter(newFile);

		dataWriter.write("Endpoint is : " + endpoint);
		dataWriter.close();
	}

}
