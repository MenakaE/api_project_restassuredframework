# API_Project_RestAssuredFramework

**About Project**

Framework designed to trigger and validate API using **RestAssured**. My Framework is combination of Keyword driven and data driven framework.


My framework can be classified into 6 components
1. Test Driving Mechanism
2. Test Scripts
3. Common Functions
4. Data files
5. Reports
6. Libraries

**1. Test Driving Mechanism**

I am using static driver class and dynamic driver class to drive my test execution
1. **Static driver Class** - Importing all my test scripts into single main method for test case execution
2. **Dynamic Driver Class** - Using **java.lang.reflect**, I can dynamically calling all the test scripts for test case execution

**2. Test Scripts**


- I am having separate test scripts for each API
- In a single test script, I am having multiple test cases for validation

**3. Common Functions**
	
**API related common function**
1. Endpoint repository - To store all endpoints of API
2. Request repository - Contains request payload of each API
3. Common function - To trigger an API and to fetch the status code and response

**Utilities related common function**
1. Utility to Create Directory - To create a directory for log files if it doesn't exist. If already exists, to delete and   create directory.
2. Utility to create log files - Once API is executed, creates a log file and adds the corresponding endpoint, request payload, response payload and headers into a text file(log file) using Filewriter class.
3. Excel data extractor utility - To read data or variables from excel file using apache poi library.
4. Extent Listener Class - Implements ITestListener to track the events that occur during execution of API tests.

**4. Data Files** - Consists of test data, which are reading from an excel file for request payload to test API

**5. Libraries** - Dependency management of libraries are handled by maven repository. Libraries used in my framework are
        
1. RestAssured - To trigger an API and to capture response
2. Jsonpath - To parse the response
3. Testng - For Validation of response using Assert class and for Orchestration of test case
4. Apache poi - To read and write data from an excel file
5. Allure-testng - To generate allure reports
6. Extentreport - To generate extent reports
7. Java generic library - Used for handling exceptions, to write log files

**6. Reports**

i.  Allure reports - Report on result of test case execution

ii. Extent reports - Html report which displays the result of test execution


**Added Files**

Added all my framework design related files into GitLab with the command line of GitBash with the following commands:

	git clone [https_link]
	git init
	git checkout -b "My_Checkin"
	git add *
	git status
	git commit -m "Adding files into repository"
	git push origin My_Checkin
